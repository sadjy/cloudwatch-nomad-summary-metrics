#!/usr/bin/env python3

import collections
import datetime
import sched
import time
import pprint
import nomad
import boto3

client_cloudwatch = boto3.client('cloudwatch', region_name='${region}')
client_nomad = nomad.Nomad(secure=True, cert=("/mnt/nomad/certs/agent.crt", "/mnt/nomad/certs/agent.key"))
summary_status = frozenset(['Queued', 'Starting', 'Running', 'Failed', 'Complete', 'Lost'])

def put_metrics(now):
    jobs = client_nomad.jobs.get_jobs()
    summ = collections.Counter({s: 0 for s in summary_status})
    for j in jobs:
        summ.update(list(j["JobSummary"]["Summary"].values())[0])

    metric_data = [
        {
            'MetricName': 'Job summary',
            'Timestamp': now,
            'Value': count,
            'Unit': 'Count',
            'Dimensions': [
                { 'Name': 'Status', 'Value': status },
            ],
        }
        for status, count in summ.items()
    ]
    pprint.pprint(metric_data)
    client_cloudwatch.put_metric_data(
        Namespace='Nomad',
        MetricData=metric_data,
    )
    enter_next(s, put_metrics)

def enter_next(s, function):
    now = datetime.datetime.utcnow()
    now_next = now.replace() + datetime.timedelta(seconds=5)
    s.enterabs(
        time=now_next.timestamp(),
        priority=1,
        action=function,
        argument=(now_next,),
    )

if __name__ == '__main__':
    s = sched.scheduler(lambda: datetime.datetime.utcnow().timestamp(), time.sleep)
    enter_next(s, put_metrics)
    put_metrics
    while True:
        s.run()
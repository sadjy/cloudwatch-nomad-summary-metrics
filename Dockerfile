FROM python:alpine

RUN pip install --no-cache-dir python-nomad boto3
COPY cloudwatch-nomad-summary-metrics.py ./cloudwatch-nomad-summary-metrics.py
CMD ["./cloudwatch-nomad-metrics.py"]
# Cloudwatch Nomad summary metrics

Export the count of 'Queued', 'Starting', 'Running', 'Failed', 'Complet', 'Lost' Nomad jobs to Cloudwatch. The script assumes it runs on a machine with a running localhost Nomad agent, and has access to AWS credentials, via environment variables, shared credentials file or EC2 instance role.

It exports to the following Cloudwatch metrics
( _Namespace/Name(dimension=value)_ ) :

 * Nomad/Job summary(Status=Queued)
 * Nomad/Job summary(Status=Starting)
 * Nomad/Job summary(Status=Failed)
 * Nomad/Job summary(Status=Complete)
 * Nomad/Job summary(Status=Lost)

Inspiration: https://github.com/trackit/cloudwatch-nomad-metrics